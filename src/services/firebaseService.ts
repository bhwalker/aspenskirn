import firestore from '@react-native-firebase/firestore'

const ref = firestore()
  .collection('cities')
  .doc('London')

async function fetchFirebase(ref: any) {
  const response = await firestore().runTransaction(
    async (transaction: any) => {
      const doc = await transaction.get(ref)
      return doc.data()
    },
  )

  return response
}

export async function getDocData(collection: string, doc: string) {
  const results = await firestore()
    .collection(collection)
    .doc(doc)
    .get()

  return results.data()
}

export async function getCollectionData(collection: string) {
  const results = await firestore()
    .collection(collection)
    .get()

  const data = []
  for (const doc of results.docs) {
    if (doc.exists) {
      data.push(doc.data())
    }
  }
  return data
}

import aspenhighlands from './aspenhighlands.png';
import aspenmountain from './aspenmountain.png';
import AspenMountainFireworks from './AspenMountainFireworks.png';
import AspenSnowStake from './AspenSnowStake.png';
import background from './background.png';
import buttermilk from './buttermilk.png';
import buttermilk2 from './buttermilk2.png';
import directions from './directions.png';
import grooming from './grooming.png';
import IMG_0134 from './IMG_0134.jpeg';
import IMG_0447 from './IMG_0447.png';
import LiftStatus from './LiftStatus.png';
import snowmass from './snowmass.png';
import snowmassicon from './snowmassicon.png';
import SnowmassSnowStake from './SnowmassSnowStake.png';
import trails from './trails.png';

export const images = {
  aspenhighlands,
  aspenmountain,
  AspenMountainFireworks,
  AspenSnowStake,
  background,
  buttermilk,
  buttermilk2,
  directions,
  grooming,
  IMG_0134,
  IMG_0447,
  LiftStatus,
  snowmass,
  snowmassicon,
  SnowmassSnowStake,
  trails
};

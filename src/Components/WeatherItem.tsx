import React from 'react'
import { StyleSheet, View, Image, Text } from 'react-native'
import { CardBase } from './CardBase'

interface Props {
  name: string
  temperature: string
  temperatureUnit: string
  windSpeed: string
  windDirection: string
  icon: string
  shortForecast: string
}

export const WeatherItem: React.FC<Props> = ({
  name,
  temperature,
  temperatureUnit,
  windSpeed,
  windDirection,
  icon,
  shortForecast,
}) => {
  return (
    <CardBase>
      <View style={styles.root}>
        <View>
          <Image source={{ uri: icon }} style={styles.image} />
        </View>
        <View>
          <Text style={styles.temp}>
            {temperature} {temperatureUnit}
          </Text>
        </View>
        <View style={styles.right}>
          <Text style={styles.title}>{name}</Text>
          <Text style={styles.subTitle}>{shortForecast}</Text>
          <Text style={styles.subTitle}>
            wind {windDirection} at {windSpeed}
          </Text>
        </View>
      </View>
    </CardBase>
  )
}

const styles = StyleSheet.create({
  root: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  right: {
    padding: 15,
    justifyContent: 'center',
  },
  temp: {
    fontSize: 30,
    fontWeight: 'bold',
  },
  title: {
    fontWeight: 'bold',
    fontSize: 15,
  },
  subTitle: {},
  image: {
    width: 50,
    height: 50,
    borderRadius: 50,
    marginHorizontal: 10,
  },
})

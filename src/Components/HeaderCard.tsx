import { useNavigation } from '@react-navigation/native'
import _ from 'lodash'
import React from 'react'
import { Image, Linking, StyleSheet, Text, View } from 'react-native'
import { TouchableHighlight } from 'react-native-gesture-handler'
import { IPageConfig, PageType } from '../interfaces'
import { CardBase } from './CardBase'

const styles = StyleSheet.create({
  text: {
    marginRight: 90,
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  subTitle: {
    fontSize: 16,
    fontWeight: 'normal',
  },
  image: {
    height: 48,
    width: 48,
    margin: 8,
    borderRadius: 3,
  },
  innerCard: {
    flexDirection: 'row',
  },
  imageContainer: {
    alignContent: 'center',
    justifyContent: 'center',
  },
  touchable: {
    color: '#DDDDDD',
  },
})

interface Props {
  item: IPageConfig
}

export const HeaderCard: React.FC<Props> = ({ item }) => {
  const navigation = useNavigation()
  return (
    <CardBase>
      <TouchableHighlight
        activeOpacity={0.6}
        underlayColor={styles.touchable.color}
        onPress={() => {
          switch (item.navigateTo) {
            case PageType.Forecast:
              navigation.navigate(item.navigateTo, {})
              break
            case PageType.WebView:
              navigation.navigate(item.navigateTo, {
                key: item.key,
              })
              break
            case PageType.LiftStatus:
              navigation.navigate(item.navigateTo, {
                mountain: item.mountain,
              })
              break
            case PageType.Link:
              Linking.openURL(item.url)
              break
            default:
              navigation.navigate(item.navigateTo, {
                mountain: item.mountain,
                title: item.name,
              })
              break
          }
        }}
      >
        <View>
          <View style={styles.innerCard}>
            <View style={styles.imageContainer}>
              {item.image ? <Image source={_.get(item, 'image', '')} style={styles.image} /> : undefined}
            </View>
            <View style={styles.text}>
              <Text style={styles.title}>{item.name}</Text>
              <Text style={styles.subTitle}>{item.description}</Text>
            </View>
          </View>
        </View>
      </TouchableHighlight>
    </CardBase>
  )
}

import React from 'react'
import { View, StyleSheet } from 'react-native'

export function CardBase({ children }: any) {
  return <View style={styles.card}>{children}</View>
}

const styles = StyleSheet.create({
  card: {
    minHeight: 45,
    alignContent: 'stretch',
    borderBottomColor: 'grey',
    borderBottomWidth: 0.5,
    margin: 5,
  },
})

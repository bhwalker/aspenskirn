import _ from 'lodash'
import React from 'react'
import { FlatList } from 'react-native'
import { IPageConfig } from '../interfaces'
import { ActivityIndicatorComponent } from './ActivityIndicator'
import { HeaderCard } from './HeaderCard'

interface Props {
  pageLinks: IPageConfig[]
}

export const ListNavigator: React.FC<Props> = ({ pageLinks }: Props) => {
  return _.isEmpty(pageLinks) ? (
    <ActivityIndicatorComponent />
  ) : (
    <FlatList
      keyExtractor={(item) => item.key}
      data={pageLinks}
      scrollIndicatorInsets={{ top: 1, bottom: 1 }}
      renderItem={({ item, index, separators }) => <HeaderCard item={item} />}
    />
  )
}

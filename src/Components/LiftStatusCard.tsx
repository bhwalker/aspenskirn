import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { ILiftStatus } from '../interfaces'
import { CardBase } from './CardBase'

interface Props {
  item: ILiftStatus
}

export const LiftStatusCard: React.FC<Props> = ({ item }) => {
  return (
    <CardBase>
      <View style={styles.container}>
        <View style={styles.left}>
          <Text
            style={
              item.status === 'Closed' ? styles.statusClosed : styles.statusOpen
            }
          >
            {item.status === 'Closed' ? 'C' : 'O'}
          </Text>
        </View>
        <View style={styles.middle}>
          <Text style={styles.subTitle}>{item.area}</Text>
          <Text style={styles.title}>{item.liftName}</Text>
        </View>
        <View style={styles.right}>
          <Text>{item.hoursOfOperation}</Text>
          <Text>{item.type}</Text>
        </View>
      </View>
    </CardBase>
  )
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
  },
  left: {
    marginHorizontal: 5,
  },
  middle: {
    flex: 1,
    marginHorizontal: 5,
  },
  right: {
    marginHorizontal: 5,
  },
  title: {
    fontSize: 14,
    fontWeight: 'bold',
  },
  subTitle: {
    fontSize: 12,
    fontWeight: 'normal',
  },
  statusOpen: {
    fontSize: 24,
    color: 'darkgreen',
    fontWeight: 'bold',
  },
  statusClosed: {
    fontSize: 24,
    color: 'maroon',
    fontWeight: 'bold',
  },
})

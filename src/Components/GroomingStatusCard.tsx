import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { CardBase } from './CardBase'

const TrailCard: React.FC<{ trail: any }> = ({ trail }) => {
  const { isOpen, isGroomed, name, difficulty } = trail
  const getGroomedColor = isGroomed => {
    if (isGroomed) {
      return { color: 'green' }
    }
    return { color: 'red' }
  }
  const getIsOpenColor = isOpen => {
    if (isOpen) {
      return { color: 'green' }
    }
    return { color: 'red' }
  }
  const getTextStyle: any = difficulty => {
    switch (difficulty) {
      case 'Easiest':
        return { color: 'green' }
        break
      case 'More Difficult':
        return { color: 'blue' }
        break
      case 'Expert Only':
        return { color: 'black' }
        break
      case 'Most Difficult':
        return { color: 'black' }
        break
      default:
        return { color: 'green' }
        break
    }
  }

  return (
    <CardBase>
      <View style={styles.container}>
        <View style={styles.statusContainer}>
          <View style={styles.left}>
            <Text
              style={{ ...styles.statusText, ...getGroomedColor(isGroomed) }}
            >
              {isGroomed ? 'Groomed' : 'Ungroomed'}
            </Text>
          </View>
          <View style={styles.left}>
            <Text style={{ ...styles.statusText, ...getIsOpenColor(isOpen) }}>
              {isOpen ? 'Open' : 'Closed'}
            </Text>
          </View>
        </View>
        <View>
          <Text style={{ ...styles.title, ...getTextStyle(difficulty) }}>
            {name}
          </Text>
        </View>
      </View>
    </CardBase>
  )
}

export const GroomingStatusSection: React.FC = ({ groomingArea }) => {
  return (
    <View>
      <Text style={styles.sectionHeader}>{groomingArea.name}</Text>
      {groomingArea.trails.map(trail => (
        <TrailCard trail={trail} />
      ))}
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  sectionHeader: {
    fontSize: 20,
    fontWeight: 'bold',
    padding: 5,
  },
  statusContainer: {
    marginHorizontal: 5,
    alignItems: 'center',
  },
  statusText: {
    fontSize: 15,
    fontWeight: 'bold',
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  left: {
    marginHorizontal: 5,
  },
})

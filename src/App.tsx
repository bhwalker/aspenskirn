import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'
import React, { Component } from 'react'
import { Platform, StyleSheet } from 'react-native'
import 'react-native-gesture-handler'
import { ExteriorNavigator } from './screens/exterior/ExteriorNavigator'

// TODO(you): import any additional firebase services that you require for your app, e.g for auth:
//    1) install the npm package: `yarn add @react-native-firebase/auth@alpha` - you do not need to
//       run linking commands - this happens automatically at build time now
//    2) rebuild your app via `yarn run run:android` or `yarn run run:ios`
//    3) import the package here in your JavaScript code: `import '@react-native-firebase/auth';`
//    4) The Firebase Auth service is now available to use here: `firebase.auth().currentUser`

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\nCmd+D or shake for dev menu',
  android: 'Double tap R on your keyboard to reload,\nShake or press menu button for dev menu',
})

const firebaseCredentials = Platform.select({
  ios: 'https://invertase.link/firebase-ios',
  android: 'https://invertase.link/firebase-android',
})

// const Stack = createStackNavigator()

type Props = {}

export default class App extends Component<Props> {
  render() {
    return (
      <NavigationContainer>
        <ExteriorNavigator />
        {/* <Stack.Navigator
          initialRouteName="Home"
          st
          // ={styles.container}
        >
          <Stack.Screen name="Home" component={ExteriorNavigator} />
          <Stack.Screen name="Notifications" component={ExteriorNavigator} />
        </Stack.Navigator> */}
        {/* {!firebase.apps.length && (
            <Text style={styles.instructions}>
              {`\nYou currently have no Firebase apps registered, this most likely means you've not downloaded your project credentials. Visit the link below to learn more. \n\n ${firebaseCredentials}`}
            </Text>
          )} */}
      </NavigationContainer>
    )
  }
}

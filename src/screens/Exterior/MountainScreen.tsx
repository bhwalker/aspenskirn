import { useNavigation, useRoute } from '@react-navigation/native'
import _ from 'lodash'
import React, { useEffect } from 'react'
import { ListNavigator } from '../../components/ListNavigator'
import { IPageConfig } from '../../interfaces'
import { pageConfigs } from './PageConfigs'

export const MountainScreen: React.FC = () => {
  const navigation = useNavigation()
  const route = useRoute()
  const { mountain, title } = route.params as any

  useEffect(() => {
    navigation.setOptions({ title })
  })

  const webCams: IPageConfig[] = _.chain(pageConfigs)
    .filter((pageConfig) => pageConfig.mountain === mountain)
    .orderBy((item) => _.toNumber(item.key))
    .value()

  return <ListNavigator pageLinks={webCams} />
}

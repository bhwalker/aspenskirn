import { useNavigation, useRoute } from '@react-navigation/native'
import _ from 'lodash'
import React, { useEffect, useState } from 'react'
import { FlatList } from 'react-native'
import { ActivityIndicatorComponent } from '../../components/ActivityIndicator'
import { LiftStatusCard } from '../../components/LiftStatusCard'
import { ILiftStatus } from '../../interfaces'
import { getDocData } from '../../services/firebaseService'

export const LiftStatusScreen: React.FC = () => {
  const navigation = useNavigation()
  const route = useRoute()
  const { mountain } = route.params as any
  const [liftStatuses, setLiftStatuses] = useState([])

  const retrievedStatuses = async () => {
    const liftStatus = (await getDocData('liftstatus', mountain)) as ILiftStatus[]
    const count = 0

    let data = []
    _.forOwn(liftStatus, (status) => {
      data.push(status)
    })

    data = _.orderBy(data, ['area', 'liftName'])

    setLiftStatuses(data)
  }

  useEffect(() => {
    navigation.setOptions({ title: `${mountain} Lift Status` })
    retrievedStatuses()
  }, [mountain])

  return _.isEmpty(liftStatuses) ? (
    <ActivityIndicatorComponent />
  ) : (
    <FlatList
      keyExtractor={(item) => item.liftName}
      data={liftStatuses}
      scrollIndicatorInsets={{ top: 1, bottom: 1 }}
      renderItem={({ item, index, separators }) => <LiftStatusCard item={item} />}
    />
  )
}

import { createStackNavigator } from '@react-navigation/stack'
import React from 'react'
import { StyleSheet } from 'react-native'
import { EventsScreen } from './EventsScreen'
import { HomeScreen } from './HomeScreen'
import { LiftStatusScreen } from './ListStatusScreen'
import { MountainScreen } from './MountainScreen'
import { WebViewScreen } from './WebViewScreen'
import { SnowForecastScreen } from './SnowForecast'
import { GroomingScreen } from './GroomingStatusScreen'

const Stack = createStackNavigator()

const styles = StyleSheet.create({
  header: {
    backgroundColor: 'red',
  },
})

export const ExteriorNavigator: React.FC = () => {
  return (
    <Stack.Navigator
      screenOptions={{
        headerTintColor: 'white',
        headerStyle: styles.header,
      }}
    >
      <Stack.Screen name="home" component={HomeScreen} options={{ headerTitle: 'Home' }} />
      <Stack.Screen name="mountain" component={MountainScreen} />
      <Stack.Screen name="events" component={EventsScreen} />
      <Stack.Screen name="webview" component={WebViewScreen} />
      <Stack.Screen name="liftstatus" component={LiftStatusScreen} />
      <Stack.Screen name="snowforecast" component={SnowForecastScreen} options={{ headerTitle: 'Snow Forecast' }} />
      <Stack.Screen name="groomingreport" component={GroomingScreen} />
    </Stack.Navigator>
  )
}

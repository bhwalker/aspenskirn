import { useNavigation, useRoute } from '@react-navigation/native'
import _ from 'lodash'
import React, { useEffect } from 'react'
import { StyleSheet } from 'react-native'
import { WebView } from 'react-native-webview'
import { pageConfigs } from './PageConfigs'

export const WebViewScreen: React.FC = () => {
  const navigation = useNavigation()
  const route = useRoute()
  const { key } = route.params as any
  const pageItem = _.find(
    pageConfigs,
    searchItem => searchItem.key === key,
  ) || {
    name: '',
    url: '',
  }

  useEffect(() => {
    navigation.setOptions({ title: pageItem.name })
  })

  return <WebView style={styles.webView} source={{ uri: pageItem.url }} />
}

const styles = StyleSheet.create({
  webView: {
    flex: 1,
  },
})

import { useNavigation, useRoute } from '@react-navigation/native'
import _ from 'lodash'
import React, { useEffect, useState } from 'react'
import { FlatList } from 'react-native'
import { ActivityIndicatorComponent } from '../../components/ActivityIndicator'
import { GroomingStatusSection } from '../../components/GroomingStatusCard'
import { getDocData } from '../../services/firebaseService'

export const GroomingScreen: React.FC = () => {
  const navigation = useNavigation()
  const route = useRoute()
  const { mountain } = route.params
  const [trailStatuses, setTrailStatuses] = useState([])

  const retrievedStatuses = async () => {
    const groomStatus = (await getDocData('groomingreport', mountain)) as ILiftStatus[]
    const count = 0

    const data = []
    _.forOwn(groomStatus, (status) => {
      data.push(status)
    })

    // data = _.orderBy(data, ['area', 'liftName'])

    setTrailStatuses(data)
  }

  useEffect(() => {
    navigation.setOptions({ title: `${mountain} Lift Status` })
    retrievedStatuses()
  }, [mountain])

  return _.isEmpty(trailStatuses) ? (
    <ActivityIndicatorComponent />
  ) : (
    <FlatList
      keyExtractor={(item) => item.name}
      data={trailStatuses}
      scrollIndicatorInsets={{ top: 1, bottom: 1 }}
      renderItem={({ item, index, separators }) => <GroomingStatusSection groomingArea={item} />}
    />
  )
}

import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs'
import _ from 'lodash'
import React, { useState } from 'react'
import { ScrollView } from 'react-native'
import WebView from 'react-native-webview'
import { ActivityIndicatorComponent } from '../../components/ActivityIndicator'
import { WeatherItem } from '../../components/WeatherItem'

const Tab = createMaterialTopTabNavigator()

const Weather: React.FC = () => {
  const [periods, setPeriods] = useState([])

  async function fetchForecast() {
    const results = await fetch('https://api.weather.gov/gridpoints/GJT/154,102/forecast')
    const resultsJson = await results.json()
    setPeriods(resultsJson.properties.periods)
  }

  React.useEffect(() => {
    let isCurrent = true

    fetchForecast()

    return () => {
      isCurrent = false
    }
  }, [1])

  return _.isEmpty(periods) ? (
    <ActivityIndicatorComponent />
  ) : (
    <ScrollView scrollIndicatorInsets={{ top: 1, bottom: 1 }}>
      {periods.map((period: any) => (
        <WeatherItem key={period.name} {...period} />
      ))}
    </ScrollView>
  )
}

const Snow: React.FC = () => {
  return (
    <WebView
      source={{
        uri: 'https://www.accuweather.com/en/us/aspen/81611/winter-weather-forecast/332154',
      }}
    />
  )
}

export const SnowForecastScreen: React.FC = () => {
  return (
    <Tab.Navigator>
      <Tab.Screen name="Weather" component={Weather} />
      <Tab.Screen name="Snow" component={Snow} />
    </Tab.Navigator>
  )
}

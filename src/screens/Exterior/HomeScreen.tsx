import React from 'react'
import { ListNavigator } from '../../components/ListNavigator'
import { IPageConfig, MountainName, PageType } from '../../interfaces'
import { images } from '../../theme/images'

const appPages: IPageConfig[] = [
  {
    key: '1',
    mountain: MountainName.Events,
    navigateTo: PageType.Events,
    name: 'Events',
    url: '/events',
    image: images.AspenMountainFireworks,
    description:
      'Here is a list of events from the Aspen/Snowmass website.  There is other stuff going on as well, but if you are looking for ski related events, this is the best place to check out.',
  },
  {
    key: '2',
    mountain: MountainName.Forecast,
    navigateTo: PageType.Forecast,
    name: 'Snow Forecast',
    url: '/snowforecast',
    image: images.snowmass,
    description:
      'Forecast temperatures and snow report.  Sometimes the snow comes early or late so who knows. Be sure to bring both your light and dark goggle lenses.  The weather changes fast.',
  },
  {
    key: '3',
    mountain: MountainName.Snowguages,
    navigateTo: PageType.Mountain,
    name: 'Snow Stakes',
    url: '/mountain/Snowguages',
    image: images.AspenSnowStake,
    description: 'Webcams pointed at our 2 snow stakes.',
  },
  {
    key: '4',
    mountain: MountainName.Aspen,
    navigateTo: PageType.Mountain,
    name: 'Aspen Mountain',
    url: `/mountain/${MountainName.Aspen.toString()}`,
    image: images.aspenmountain,
    description: 'The hardest place to ski.  Intermediate trails only.',
  },
  {
    key: '5',
    mountain: MountainName.Highlands,
    navigateTo: PageType.Mountain,
    name: 'Aspen Highlands',
    url: `/mountain/${MountainName.Highlands.toString()}`,
    image: images.aspenhighlands,
    description: 'The 2nd hardest place to ski with a legendary bowl.',
  },
  {
    key: '6',
    mountain: MountainName.Buttermilk,
    navigateTo: PageType.Mountain,
    name: 'Buttermilk',
    url: `/mountain/${MountainName.Buttermilk.toString()}`,
    image: images.buttermilk2,
    description: 'A snowboarder and beginners paradise.  Host to the annual Aspen X Games.',
  },
  {
    key: '7',
    mountain: MountainName.Snowmass,
    navigateTo: PageType.Mountain,
    name: 'Snowmass',
    url: `/mountain/${MountainName.Snowmass.toString()}`,
    image: images.snowmassicon,
    description: 'A mountain with something for everyone.',
  },
]

export const HomeScreen: React.FC = () => {
  return <ListNavigator pageLinks={appPages} />
}

import { useNavigation } from '@react-navigation/native'
import React, { useEffect, useState } from 'react'
import { ListNavigator } from '../../components/ListNavigator'
import { getCollectionData } from '../../services/firebaseService'
import { PageType, MountainName } from '../../interfaces'
import moment from 'moment'

export const EventsScreen: React.FC = () => {
  const navigation = useNavigation()
  const [events, setEvents] = useState([])

  const getEvents = async () => {
    const events = await getCollectionData('events')
    const count = 0

    const list = events.map((event, index) => {
      return {
        key: index,
        name: event.name,
        url: event.url,
        navigateTo: PageType.Link,
        mountain: MountainName.Events,
        image: event.image,
        description: `${event.cost} ${moment(event.startDate).calendar()} at ${event.location}`,
      }
    })

    setEvents(list)
  }

  useEffect(() => {
    navigation.setOptions({ title: 'Events' })
    getEvents()
  })

  return <ListNavigator pageLinks={events} />
}

import { IPageConfig, MountainName, PageType } from '../../interfaces'
import { images } from '../../theme/images'

export const pageConfigs: IPageConfig[] = [
  {
    key: '10',
    name: 'Aspen Snow Stake',
    sourceWebsite: 'ipcamlive.com',
    url: 'https://g1.ipcamlive.com/player/player.php?alias=5c8193f8ba576&skin=white&autoplay=1',
    navigateTo: PageType.WebView,
    mountain: MountainName.Snowguages,
    image: images.AspenSnowStake,
    description: 'Webcam of the Aspen snow stake.  This is usually the first place you will see snow when it comes in.',
  },
  {
    key: '20',
    name: 'Snomass Snow Stake',
    sourceWebsite: 'https://www.aspensnowmass.com/our-mountains/snowmass/mountain-cams/snow-stake-time-lapse',
    url: 'https://g1.ipcamlive.com/player/player.php?alias=5c6d8ba5a8388&skin=white&autoplay=1',
    navigateTo: PageType.WebView,
    mountain: MountainName.Snowguages,
    image: images.SnowmassSnowStake,
    description:
      'Webcam of the Snowmass snow stake.  This is a good place to see how much snow is falling.  It gets a lot of sun so sometimes it is snowing, but all the snow on the stake melts.',
  },
  {
    key: '23',
    name: '',
    sourceWebsite:
      'https://www.google.com/maps/dir//Buttermilk+Ski+Resort,+38700+CO-82,+Aspen,+CO+81611/@39.2282155,-106.8967772,13z/data=!3m1!4b1!4m10!4m9!1m1!4e1!1m5!1m1!1s0x874039628fa19387:0x4e941994c13e1098!2m2!1d-106.8604932!2d39.2057944!3e0',
    url:
      'https://www.google.com/maps/dir//Buttermilk+Ski+Resort,+38700+CO-82,+Aspen,+CO+81611/@39.2282155,-106.8967772,13z/data=!3m1!4b1!4m10!4m9!1m1!4e1!1m5!1m1!1s0x874039628fa19387:0x4e941994c13e1098!2m2!1d-106.8604932!2d39.2057944!3e0',
    navigateTo: PageType.Link,
    mountain: MountainName.Buttermilk,
    image: images.directions,
    description: 'Google map directions to Buttermilk.  Hit the bus button to get the bus routes.',
  },
  // {
  //   key: '24',
  //   name: 'Family Tips',
  //   sourceWebsite: '',
  //   url: '',
  //   navigateTo: PageType.DescPage,
  //   mountain: MountainName.Buttermilk,
  //   image: images.directions,
  //   description:
  //     'Google map directions to Buttermilk.  Hit the bus button to get the bus routes.',
  //   // pageDesc: () => {
  //   //   return (
  //   //     <IonCard>
  //   //       <IonCardHeader>
  //   //         <IonCardSubtitle>The Family Mountain of Apsen</IonCardSubtitle>
  //   //         <IonCardTitle>Family Tips</IonCardTitle>
  //   //       </IonCardHeader>

  //   //       <IonCardContent>
  //   //         <IonImg src="https://www.aspensnowmass.com/-/media/aspensnowmass/ski-school/2018-lessons/2018-child-group-lessons-34-image-cta-33-m.ashx?h=200&w=750&hash=EDDB7FD4B32E3B47DB59A9805E8C3F5C372ACA42"></IonImg>
  //   //         <p>
  //   //           This mountain is setup for kkeys whether you want to drop them off
  //   //           or go ski with them. If you are skiing with them, West Buttermilk
  //   //           is a great place to go. They have a mkey lift so you can stay on
  //   //           the green slopes. The mkey lift also tends to get less crowded than
  //   //           the bottom of West Buttermilk.
  //   //         </p>
  //   //         <br />
  //   //         <p>
  //   //           This mountain is setup to take care of all ages of kkeys. There is
  //   //           an area at the base of the mountain that accomodates really small
  //   //           children.
  //   //         </p>
  //   //         <br />
  //   //         <p>
  //   //           Once the students are older, they go all over the mountain. If you
  //   //           want to see ears on helmets or skiers in full furry jumpsuits,
  //   //           this is the mountain.
  //   //         </p>
  //   //       </IonCardContent>
  //   //     </IonCard>
  //   //   );
  //   // }
  // },
  {
    key: '25',
    name: 'Lift Status',
    sourceWebsite: 'https://www.aspensnowmass.com/our-mountains/buttermilk/lift-status',
    url: 'https://www.aspensnowmass.com/our-mountains/buttermilk/lift-status',
    navigateTo: PageType.LiftStatus,
    mountain: MountainName.Buttermilk,
    image: images.LiftStatus,
    description: 'Lift status for buttermilk.',
  },
  {
    key: '26',
    name: 'Trail Map',
    sourceWebsite:
      'https://www.aspensnowmass.com/-/media/aspensnowmass/trail-maps/1920/1920-buttermilk-website-map-without-klaus-way.ashx',
    url:
      'https://www.aspensnowmass.com/-/media/aspensnowmass/trail-maps/1920/1920-buttermilk-website-map-without-klaus-way.ashx',
    navigateTo: PageType.WebView,
    mountain: MountainName.Buttermilk,
    image: images.trails,
    description:
      'Zoomable map of the trails.  Summit has some great trails but is the slowest lift.  West Buttermilk is a great place to start in the morning until all the students show up.  If you are searching for powder, TieHack runs are often the best place to look but hit it first before the runs get chopped up.',
  },
  {
    key: '27',
    name: 'Grooming Report',
    sourceWebsite: 'https://www.aspensnowmass.com/our-mountains/buttermilk/snow-and-grooming-report',
    url: 'https://www.aspensnowmass.com/our-mountains/buttermilk/snow-and-grooming-report',
    navigateTo: PageType.GroomingReport,
    mountain: MountainName.Buttermilk,
    image: images.grooming,
    description:
      'This is a great place to check out to see which TieHack runs have been groomed.  The rest of the mountain gets regular grooming, but the TieHack runs are often alternated ever other day.',
  },
  {
    key: '30',
    name: 'Buttermilk Cam',
    sourceWebsite: 'https://aspensnowmass.com',
    url: 'http://aspen.roundshot.com/buttermilk/',
    navigateTo: PageType.WebView,
    mountain: MountainName.Buttermilk,
    image: images.snowmass,
    description: 'Check out the top of Buttermilk',
  },
  {
    key: '40',
    name: 'Summit Express',
    url: 'https://www.aspensnowmass.com/camimages/Aspen_View.jpg',
    sourceWebsite: 'https://www.aspensnowmass.com/our-mountains/buttermilk/mountain-cams',
    navigateTo: PageType.WebView,
    mountain: MountainName.Buttermilk,
    image: images.snowmass,
    description: 'Great cam to check out the crowds, but lets be honest, Aspen nevers has lines even during Christmas.',
  },
  {
    key: '50',
    name: 'Superpipe',
    url: 'https://www.aspensnowmass.com/camimages/superpipe.jpg',
    sourceWebsite: 'https://www.aspensnowmass.com/our-mountains/buttermilk/mountain-cams',
    navigateTo: PageType.WebView,
    mountain: MountainName.Buttermilk,
    image: images.snowmass,
    description: "I can't say this webcam is of much use unless there is an event going on.",
  },
  {
    key: '60',
    name: 'Bumps Restaurant',
    url: 'https://www.aspensnowmass.com/camimages/Bumps.jpg',
    sourceWebsite: 'https://www.aspensnowmass.com/our-mountains/buttermilk/mountain-cams',
    navigateTo: PageType.WebView,
    mountain: MountainName.Buttermilk,
    image: images.snowmass,
    description: 'This webcam show how crowded it is outskeye of Bumps restuarant',
  },
  {
    key: '63',
    name: 'Directions',
    sourceWebsite:
      'https://www.google.com/maps/dir//Aspen+Highlands+Ski+Resort,+199+Prospector+Rd,+Aspen,+CO+81611/@39.2036506,-106.8630685,14z/data=!3m1!4b1!4m10!4m9!1m1!4e1!1m5!1m1!1s0x8740391ae85815fb:0x51349e172e6ecde0!2m2!1d-106.8553613!2d39.1824124!3e0',
    url:
      'https://www.google.com/maps/dir//Aspen+Highlands+Ski+Resort,+199+Prospector+Rd,+Aspen,+CO+81611/@39.2036506,-106.8630685,14z/data=!3m1!4b1!4m10!4m9!1m1!4e1!1m5!1m1!1s0x8740391ae85815fb:0x51349e172e6ecde0!2m2!1d-106.8553613!2d39.1824124!3e0',
    navigateTo: PageType.Link,
    mountain: MountainName.Aspen,
    image: images.directions,
    description: '',
  },
  {
    key: '64',
    name: 'Lift Status',
    url: 'https://www.aspensnowmass.com/our-mountains/aspen-mountain/lift-status',
    sourceWebsite: 'https://www.aspensnowmass.com/our-mountains/aspen-mountain/lift-status',
    navigateTo: PageType.LiftStatus,
    mountain: MountainName.Aspen,
    image: images.LiftStatus,
    description: '',
  },
  {
    key: '65',
    name: 'Trail Map',
    url: 'https://www.aspensnowmass.com/-/media/aspensnowmass/trail-maps/1920/2019-aspen-mountain-website-map.ashx',
    sourceWebsite:
      'https://www.aspensnowmass.com/-/media/aspensnowmass/trail-maps/1920/2019-aspen-mountain-website-map.ashx',
    navigateTo: PageType.WebView,
    mountain: MountainName.Aspen,
    image: images.trails,
    description: '',
  },
  {
    key: '66',
    name: 'Grooming Report',
    url: 'https://www.aspensnowmass.com/our-mountains/aspen-mountain/snow-and-grooming-report',
    sourceWebsite: 'https://www.aspensnowmass.com/our-mountains/aspen-mountain/snow-and-grooming-report',
    navigateTo: PageType.GroomingReport,
    mountain: MountainName.Aspen,
    image: images.grooming,
    description: '',
  },
  {
    key: '70',
    name: 'Aspen Snow Stake',
    sourceWebsite: 'ipcamlive.com',
    url: 'https://g1.ipcamlive.com/player/player.php?alias=5c8193f8ba576&skin=white&autoplay=1',
    navigateTo: PageType.WebView,
    mountain: MountainName.Aspen,
    image: images.AspenSnowStake,
    description: '',
  },
  {
    key: '80',
    name: 'Gauges',
    sourceWebsite: 'https://www.aspensnowmass.com/our-mountains/aspen-mountain/mountain-cams',
    url: 'https://www.aspensnowmass.com/camimages/Gauges.jpg',
    navigateTo: PageType.WebView,
    mountain: MountainName.Aspen,
    image: images.snowmass,
    description: '',
  },
  {
    key: '90',
    name: 'Gondola',
    sourceWebsite: 'https://aspenwebcam.com/live-webcams/aspen-mountain-gondola-plaza',
    url: 'https://c.streamhoster.com/embed/media/O7sB36/JwHtJZnRsLg/KmYtJDscTlT_5',
    navigateTo: PageType.WebView,
    mountain: MountainName.Aspen,
    image: images.snowmass,
    description: '',
  },
  {
    key: '110',
    name: 'Ajax',
    sourceWebsite: 'https://aspenwebcam.com/live-webcams/aspen-mountain-ajax',
    url: 'https://c.streamhoster.com/embed/media/O7sB36/JwHtJZnRsLg/KmYe2ascTlM_5',
    navigateTo: PageType.WebView,
    mountain: MountainName.Aspen,
    image: images.snowmass,
    description: '',
  },
  {
    key: '120',
    name: 'Aztec and Shadow Mountain',
    sourceWebsite: 'https://aspenwebcam.com/live-webcams/aspen-colorado-aztec-and-shadow-mountain',
    url: 'https://c.streamhoster.com/embed/media/O7sB36/JwHtJZnRsLg/Km0iwxscTlD_5',
    navigateTo: PageType.WebView,
    mountain: MountainName.Aspen,
    image: images.snowmass,
    description: '',
  },
  {
    key: '130',
    name: 'Power of Four',
    sourceWebsite: 'https://aspensnowmass.com',
    url: 'http://aspen.roundshot.com/power-of-four/?_ga=2.173453925.773282619.1571604393-610760258.1570378484',
    navigateTo: PageType.WebView,
    mountain: MountainName.Aspen,
    image: images.snowmass,
    description: '',
  },
  {
    key: '140',
    name: 'Aspen Mountain',
    sourceWebsite: 'https://aspensnowmass.com',
    url: 'http://aspen.roundshot.com/aspen/?_ga=2.246143108.1219595349.1572190743-610760258.1570378484',
    navigateTo: PageType.WebView,
    mountain: MountainName.Aspen,
    image: images.snowmass,
    description: '',
  },
  {
    key: '143',
    name: 'Directions',
    sourceWebsite:
      'https://www.google.com/maps/dir//Aspen+Highlands+Ski+Resort,+Prospector+Road,+Aspen,+CO/@39.2036506,-106.8630685,14z/data=!3m1!4b1!4m10!4m9!1m1!4e1!1m5!1m1!1s0x8740391ae85815fb:0x51349e172e6ecde0!2m2!1d-106.8553613!2d39.1824124!3e0',
    url:
      'https://www.google.com/maps/dir//Aspen+Highlands+Ski+Resort,+Prospector+Road,+Aspen,+CO/@39.2036506,-106.8630685,14z/data=!3m1!4b1!4m10!4m9!1m1!4e1!1m5!1m1!1s0x8740391ae85815fb:0x51349e172e6ecde0!2m2!1d-106.8553613!2d39.1824124!3e0',
    navigateTo: PageType.Link,
    mountain: MountainName.Highlands,
    image: images.directions,
    description: '',
  },
  {
    key: '145',
    name: 'Lift Status',
    sourceWebsite: 'https://www.aspensnowmass.com/our-mountains/aspen-highlands/lift-status',
    url: 'https://www.aspensnowmass.com/our-mountains/aspen-highlands/lift-status',
    navigateTo: PageType.LiftStatus,
    mountain: MountainName.Highlands,
    image: images.LiftStatus,
    description: '',
  },
  {
    key: '146',
    name: 'Trail Maps',
    sourceWebsite:
      'https://www.aspensnowmass.com/-/media/aspensnowmass/trail-maps/1920/2019-highlands-website-map.ashx',
    url: 'https://www.aspensnowmass.com/-/media/aspensnowmass/trail-maps/1920/2019-highlands-website-map.ashx',
    navigateTo: PageType.WebView,
    mountain: MountainName.Highlands,
    image: images.trails,
    description: '',
  },
  {
    key: '147',
    name: 'Grooming Report',
    sourceWebsite: 'https://www.aspensnowmass.com/our-mountains/aspen-highlands/snow-and-grooming-report',
    url: 'https://www.aspensnowmass.com/our-mountains/aspen-highlands/snow-and-grooming-report',
    navigateTo: PageType.GroomingReport,
    mountain: MountainName.Highlands,
    image: images.grooming,
    description: '',
  },
  {
    key: '150',
    name: 'Aspen Highlands Cam',
    sourceWebsite: 'https://www.aspensnowmass.com/our-mountains/buttermilk/mountain-cams',
    url: 'http://aspen.roundshot.com/highlands/',
    navigateTo: PageType.WebView,
    mountain: MountainName.Highlands,
    image: images.snowmass,
    description: '',
  },
  {
    key: '160',
    name: 'Highland Bowl',
    sourceWebsite: 'https://www.aspensnowmass.com/our-mountains/aspen-highlands/mountain-cams',
    url: 'https://backend.roundshot.com/cams/3d9af2719f79f6c4447593172a4f5c6a/medium',
    navigateTo: PageType.WebView,
    mountain: MountainName.Highlands,
    image: images.snowmass,
    description: '',
  },
  {
    key: '170',
    name: 'Maroon Bell',
    sourceWebsite: 'https://www.aspensnowmass.com/our-mountains/aspen-highlands/mountain-cams',
    url: 'https://backend.roundshot.com/cams/19a0db224d24e8f0b9f874eeb6b18a9a/medium',
    navigateTo: PageType.WebView,
    mountain: MountainName.Highlands,
    image: images.snowmass,
    description: '',
  },
  {
    key: '173',
    name: 'Directions',
    sourceWebsite:
      'https://www.google.com/maps/dir//Snowmass+Village,+Colorado/data=!4m7!4m6!1m1!4e2!1m2!1m1!1s0x87404876f56b69d5:0x7191f01543f2a1bd!3e0?sa=X&ved=2ahUKEwj-nMHK87bnAhVZVc0KHW2TAz0Q-A8wAHoECAwQDQ',
    url:
      'https://www.google.com/maps/dir//Snowmass+Village,+Colorado/data=!4m7!4m6!1m1!4e2!1m2!1m1!1s0x87404876f56b69d5:0x7191f01543f2a1bd!3e0?sa=X&ved=2ahUKEwj-nMHK87bnAhVZVc0KHW2TAz0Q-A8wAHoECAwQDQ',
    navigateTo: PageType.Link,
    mountain: MountainName.Snowmass,
    image: images.directions,
    description: '',
  },
  {
    key: '174',
    name: 'Lift Status',
    sourceWebsite: 'https://www.aspensnowmass.com/our-mountains/snowmass/lift-status',
    url: 'https://www.aspensnowmass.com/our-mountains/snowmass/lift-status',
    navigateTo: PageType.LiftStatus,
    mountain: MountainName.Snowmass,
    image: images.LiftStatus,
    description: '',
  },
  {
    key: '175',
    name: 'Trail Map',
    sourceWebsite: 'https://www.aspensnowmass.com',
    url: 'https://www.aspensnowmass.com/-/media/aspensnowmass/trail-maps/1920/2019-snowmass-website-map.ashx',
    navigateTo: PageType.WebView,
    mountain: MountainName.Snowmass,
    image: images.trails,
    description: '',
  },
  {
    key: '176',
    name: 'Grooming Report',
    sourceWebsite: 'https://www.aspensnowmass.com/our-mountains/snowmass/snow-and-grooming-report',
    url: 'https://www.aspensnowmass.com/our-mountains/snowmass/snow-and-grooming-report',
    navigateTo: PageType.GroomingReport,
    mountain: MountainName.Snowmass,
    image: images.grooming,
    description: '',
  },
  {
    key: '180',
    name: 'Snomass Snow Stake',
    sourceWebsite: 'https://www.aspensnowmass.com/our-mountains/snowmass/mountain-cams/snow-stake-time-lapse',
    url: 'https://g1.ipcamlive.com/player/player.php?alias=5c6d8ba5a8388&skin=white&autoplay=1',
    navigateTo: PageType.WebView,
    mountain: MountainName.Snowmass,
    image: images.SnowmassSnowStake,
    description: '',
  },

  {
    key: '190',
    name: 'Village Ski Hill',
    sourceWebsite: 'https://www.coloradowebcam.net',
    url: 'https://c.streamhoster.com/embed/media/O7sB36/6WW26anes7e/KmHQ2IscTlx_5',
    navigateTo: PageType.WebView,
    mountain: MountainName.Snowmass,
    image: images.snowmass,
    description: '',
  },
  {
    key: '200',
    name: 'Snowmass Village',
    sourceWebsite: 'https://www.coloradowebcam.net',
    url: 'https://c.streamhoster.com/embed/media/O7sB36/6WW26anes7e/Kmo0wCscTlK_5',
    navigateTo: PageType.WebView,
    mountain: MountainName.Snowmass,
    image: images.snowmass,
    description: '',
  },
  {
    key: '210',
    name: 'Base Village',
    sourceWebsite: 'https://aspensnowmass.com',
    url: 'https://www.aspensnowmass.com/CamImages/SM_1.jpg',
    navigateTo: PageType.WebView,
    mountain: MountainName.Snowmass,
    image: images.snowmass,
    description: '',
  },
  {
    key: '220',
    name: 'Elk Camp',
    sourceWebsite: 'https://aspensnowmass.com',
    url: 'http://aspen.roundshot.com/snowmass?_ga=2.250261766.1219595349.1572190743-610760258.1570378484',
    navigateTo: PageType.WebView,
    mountain: MountainName.Snowmass,
    image: images.snowmass,
    description: '',
  },
  {
    key: '230',
    name: "Sam's Knob",
    sourceWebsite: 'https://aspensnowmass.com',
    url: 'https://aspen.roundshot.com/snowmass-samsknob',
    navigateTo: PageType.WebView,
    mountain: MountainName.Snowmass,
    image: images.snowmass,
    description: '',
  },
  {
    key: '240',
    name: 'Events',
    sourceWebsite: 'https://www.aspensnowmass.com/while-you-are-here/events',
    url: 'https://www.aspensnowmass.com/while-you-are-here/events',
    navigateTo: PageType.Events,
    mountain: MountainName.Events,
    image: images.snowmass,
    description:
      'Here is a list of events from the Aspen/Snowmass website.  There is other stuff going on as well, but if you are looking for ski related events, this is the best place to check out.',
  },
]

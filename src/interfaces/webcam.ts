export enum PageType {
  Events = 'events',
  WebView = 'webview',
  Link = 'link',
  DescPage = 'descpage',
  LiftStatus = 'liftstatus',
  GroomingReport = 'groomingreport',
  Mountain = 'mountain',
  Forecast = 'snowforecast',
}

export enum MountainName {
  Snowmass = 'Snowmass',
  Aspen = 'AspenMountain',
  Highlands = 'AspenHighlands',
  Buttermilk = 'Buttermilk',
  Snowguages = 'Snowguages',
  Events = 'Events',
  Home = 'Home',
  Forecast = 'Forecast',
}

export interface IPageConfig {
  key: string
  name: string
  sourceWebsite?: string
  url: string
  navigateTo: PageType
  mountain: MountainName
  image: string
  description: string
  pageDesc?: Function
}

export interface ILiftStatus {
  area: string
  elevationGainFeet: string
  elevationGainMeters: string
  hoursOfOperation: string
  liftName: string
  status: string
  time: string
  type: string
}
